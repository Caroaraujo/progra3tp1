package interfaz;

public class Tablero {
	
	private int[][] tablero = new int[4][4];

	
	public Tablero() {
		
	}
	// imprime la grilla
	public int imprimir() {
		for(int i = 0; i<tablero.length ; i++) {
			for(int j = 0; j<tablero[i].length ; j++) {
				System.out.print(tablero[i][j] + "  ");
			}
			System.out.println("\n");
		}
		return 0;
	}

	// funcion que pone en 0 toda la grilla 
	public void reiniciar (boolean tab, boolean puntos) {
		
		//Reiniciar tablero
		{
			for(int i = 0; i<tablero.length ; i++) {
				for(int j = 0; j<tablero[i].length ; j++) {
					tablero[i][j] = 0;
				}
			}
		}
			
	}
	
	// retorna verdadero en caso de que en alguna posicion se logre el 2048
	public boolean verificarResultado() {
		for(int i = 0; i<tablero.length ; i++) {
			for(int j = 0; j<tablero[i].length ; j++) {
				if(tablero[i][j] == 2048) {
					return true;}
			}
		}
		return false;
	}
	

// verificamos que la matriz tenga elemento cero es decir lugar disponible para ingresar dato
public boolean casilleroDisponible(int pos1,int pos2) {
	if(tablero[pos1][pos2] == 0) {
	return true;
	}
	return false;
}
//metodo que llama a numero aleatorio // el else esta de mas lo puse para ver que no encontro lugar dispoble (despuesborrar)
public void marcarCasillero(int pos1,int pos2) {
	if(casilleroDisponible(pos1, pos2)) {
	tablero[pos1][pos2] = NumAleatorio();
	}
	else
		System.out.println("no hay lugar disponble para marcar");
}
//genera un numero random puede ser un 2 o un 4
public int NumAleatorio () {
	int numero = (int)(Math. random()*4+1);
	if (numero == 3 || numero == 1 ){
		return NumAleatorio();
	}
	return numero;
}
// nos devuelve una posicion dentro de la grilla de forma aleatoria
public int[] posRandom () {
	int[] tabl = null;
	tabl = new int [2];
	int num1 = (int)(Math. random()*4+0);
	int num2 = (int)(Math. random()*4+0);
	tabl[0] = num1;
	tabl[1] = num2;
	return tabl ;

	}
}


