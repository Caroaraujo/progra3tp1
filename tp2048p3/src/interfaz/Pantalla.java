package interfaz;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Pantalla {

	protected static final boolean KeyEvent_up = false;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla window = new Pantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 556, 502);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel paneldeljuego = new JPanel();
		paneldeljuego.setBorder(null);
		paneldeljuego.setBounds(33, 35, 483, 373);
		paneldeljuego.setVisible(false);
		frame.getContentPane().add(paneldeljuego);
		paneldeljuego.setLayout(null);
	

		JButton btnNewButton1 = new JButton("");
		btnNewButton1.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton1.setBounds(10, 11, 106, 78);
		paneldeljuego.add(btnNewButton1);

		JButton btnNewButton2 = new JButton("");
		btnNewButton2.setBounds(126, 11, 106, 78);
		paneldeljuego.add(btnNewButton2);

		JButton btnNewButton3 = new JButton("");
		btnNewButton3.setBounds(242, 11, 106, 78);
		paneldeljuego.add(btnNewButton3);

		JButton btnNewButton5 = new JButton("");
		btnNewButton5.setBounds(10, 100, 106, 78);
		paneldeljuego.add(btnNewButton5);

		JButton btnNewButton9 = new JButton("");
		btnNewButton9.setBounds(10, 189, 106, 78);
		paneldeljuego.add(btnNewButton9);

		JButton btnNewButton13 = new JButton("");
		btnNewButton13.setBounds(10, 278, 106, 78);
		paneldeljuego.add(btnNewButton13);

		JButton btnNewButton4 = new JButton("");
		btnNewButton4.setBounds(358, 11, 106, 78);
		paneldeljuego.add(btnNewButton4);

		JButton btnNewButton6 = new JButton("");
		btnNewButton6.setBounds(126, 100, 106, 78);
		paneldeljuego.add(btnNewButton6);

		JButton btnNewButton10 = new JButton("");
		btnNewButton10.setBounds(126, 189, 106, 78);
		paneldeljuego.add(btnNewButton10);

		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBounds(126, 278, 106, 78);
		paneldeljuego.add(btnNewButton_5);

		JButton btnNewButton7 = new JButton("");
		btnNewButton7.setBounds(242, 100, 106, 78);
		paneldeljuego.add(btnNewButton7);

		JButton btnNewButton8 = new JButton("");
		btnNewButton8.setBounds(358, 100, 106, 78);
		paneldeljuego.add(btnNewButton8);

		JButton btnNewButton11 = new JButton("");
		btnNewButton11.setBounds(242, 189, 106, 78);
		paneldeljuego.add(btnNewButton11);

		JButton btnNewButton12 = new JButton("");
		btnNewButton12.setBounds(358, 189, 106, 78);
		paneldeljuego.add(btnNewButton12);

		JButton btnNewButton14 = new JButton("");
		btnNewButton14.setBounds(242, 278, 106, 78);
		paneldeljuego.add(btnNewButton14);

		JButton btnNewButton15 = new JButton("");
		btnNewButton15.setBounds(358, 278, 106, 78);
		paneldeljuego.add(btnNewButton15);
		
		

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Pantalla.class.getResource("/interfaz/fondo-de-numeros.png")));
		lblNewLabel.setBounds(0, 0, 540, 463);
		frame.getContentPane().add(lblNewLabel);

		JPanel paneldenicio = new JPanel();
		paneldenicio.setBounds(10, 20, 520, 414);
		frame.getContentPane().add(paneldenicio);
		paneldenicio.setLayout(null);
		


		JButton btnComenzar = new JButton("Comenzar Juego");
		btnComenzar.setForeground(UIManager.getColor("Button.foreground"));
		btnComenzar.setFont(new Font("Times New Roman", Font.ITALIC, 22));
		btnComenzar.setBackground(UIManager.getColor("Button.darkShadow"));
		paneldenicio.add(btnComenzar);
		btnComenzar.setBounds(160, 155, 200, 80);
		btnComenzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				paneldeljuego.setVisible(true);
				btnComenzar.setVisible(false);
				
			}
			
		});
	}
}


